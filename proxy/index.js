const express = require('express');
const morgan = require("morgan");
const { createProxyMiddleware } = require('http-proxy-middleware');
const request = require('request');
const cors = require('cors');

const app = express();
require('dotenv').config();
app.use(cors());
app.use(morgan('dev'));

// Info GET endpoint
app.get('/api/info', (req, res, next) => {
    res.send('This is a proxy service.');
});

app.get('/api/test-con', (req, res, next) => {
    console.log('call test con');
    request({
        url: `${process.env.API_PATH}/tts/test`,
        method: "GET",
        time: true
    }, function (err, res2, body) {
        if (!err && res2.statusCode == 200) {
            console.log(body);
            res.send('success');
        } else {
            console.log("error : ", err);
            res.send('fail');
        }
    });
});

// Proxy endpoints
app.use('/api', createProxyMiddleware({
    target: process.env.API_PATH,
    changeOrigin: true,
    pathRewrite: {
        [`^/api`]: '',
    },
}));

app.listen(process.env.PORT, process.env.HOST, () => {
    console.log(`Starting Proxy at ${process.env.HOST}:${process.env.PORT}`);
});