import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TtsService {
  constructor(private http: HttpClient) {}

  testCallTTS(): Observable<any> {
    return this.http.get(`${environment.proxyPath}/tts/test`).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  testCallTTS2(): Observable<any> {
    return this.http.get(`${environment.proxyPath}/info`).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  testCallTTS3(): Observable<any> {
    return this.http.get(`${environment.proxyPath}/test-con`).pipe(
      map((res: any) => {
        return res;
      })
    );
  }
}
