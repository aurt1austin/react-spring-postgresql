import { TtsService } from './services/tts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'tts';
  loading = false;

  constructor(private ttsService: TtsService) {}

  ngOnInit(): void {
    this.loading = true;
    this.ttsService.testCallTTS().subscribe((res) => {
      console.log(' 1 ');
      console.log(res);
    });

    this.ttsService.testCallTTS2().subscribe((res) => {
      console.log(' 2 ');
      console.log(res);
    });

    this.ttsService.testCallTTS3().subscribe((res) => {
      console.log(' 3 ');
      console.log(res);
    });
    this.loading = false;
  }
}
