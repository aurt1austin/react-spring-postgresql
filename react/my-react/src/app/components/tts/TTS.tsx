import { useEffect } from "react";

export const TTSLayout = () => {
    useEffect(() => {
        fetch("/api/test-con")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log('useEffect result : ', result)
                },
                (error) => {
                    console.error('useEffect error : ', error)
                }
            )
    }, []);

    return (
        <div className="tts-layout p-grid">
            <div className="p-col-4">left menu</div>
            <div className="p-col-8">
                <div className="p-grid">
                    <div className="p-col-12">header</div>
                </div>
                <div className="p-grid">
                    <div className="p-col-12">body</div>
                </div>
            </div>
        </div>
    );
};
