import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.css";
import { TTSLayout } from "./app/components/tts/TTS";

function App() {
  return (
    <div className="App">
      <TTSLayout />
    </div>
  );
}

export default App;
